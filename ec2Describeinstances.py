import boto3
#Single Line Comment Syntax
""" IList function describes the instances list in the region provided by the user. 
    If Instances not available in the region, it will display the error message. 
"""
def DescribeInstances(Region):
     try:
      ec2 = boto3.client('ec2',Region)
      responses = ec2.describe_instances()
      reservations = responses['Reservations'][0]['Instances']
      instanceIDList = []
      serverNameList = []
      for i in reservations:
       instanceID = ([i][0]['InstanceId'])
       serverName = ([i][0]['Tags'][0]['Value'])
       instanceIDList.append(instanceID)
       serverNameList.append(serverName)
      print("Instance ID's are ",instanceIDList)
      print("Server Names are ",serverNameList)       
     except:
      print("No Instances Available in the {} region".format(Region)) 
     return instanceIDList
     
DescribeInstances('us-east-1')

