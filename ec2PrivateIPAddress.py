import boto3
def Getec2privateIPAddress(Region, instanceID):
    try:
     ec2 = boto3.client('ec2',Region)
     responses = ec2.describe_instances(InstanceIds=[instanceID]);
     instanceIP = responses['Reservations'][0]['Instances'][0]['PrivateIpAddress']
     print(instanceIP)    
    except:      
        print("No Instances Available in the {} region".format(Region))     
    return instanceIP; 
    
Getec2privateIPAddress('us-east-1', 'i-0bcb54345aeca3d3e')