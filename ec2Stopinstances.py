import boto3
#Single Line Comment Syntax
""" IList function describes the instances list in the region provided by the user. 
    If Instances not available in the region, it will display the error message. 
"""
def DescribeInstances(Region):
     try:
      ec2 = boto3.client('ec2',Region)
      responses = ec2.describe_instances()
      reservations = responses['Reservations'][0]['Instances']
      instanceIDList = []
      serverNameList = []
      for i in reservations:
       instanceID = ([i][0]['InstanceId'])
       serverName = ([i][0]['Tags'][0]['Value'])
       instanceIDList.append(instanceID)
       serverNameList.append(serverName)
      #print("Instance ID's are ",instanceIDList)
      #print("Server Names are ",serverNameList)       
     except:
      print("No Instances Available in the {} region".format(Region)) 
     return instanceIDList


""" Reboot Instances function takes Instane IDs list as argument and reboot the instances
"""
def RebootInstances(InstanceIDs,Region):
    try:
     ec2 = boto3.client('ec2',Region)
     for instanceid in InstanceIDs:     
      #response = ec2.stop_instances( InstanceIds = 'i-03d1888dbacc7dde6')      
      response = ec2.stop_instances( InstanceIds = instanceid.split(" "))
      statusCode = response['StoppingInstances'][0]['CurrentState']['Code']
      if(statusCode==64):
        print("Instance ID -> {} stopped succesfully".format(instanceid))
      else:
       print("Instance ID -> {} stopped operation aborted ".format(instanceid))        
    except NameError:
     print("Instance ID -> {} stopped operation aborted with error {} ".format(instanceid,NameError))           
    
#Function Execution 
RebootInstances(DescribeInstances('us-east-1'),'us-east-1')
